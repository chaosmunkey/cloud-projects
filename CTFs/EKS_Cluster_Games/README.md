# [Wiz EKS Challenges](https://eksclustergames.com/)

These are a set of Kubernetes CTF challenges hosted by [Wiz](https://www.wiz.io/blog/announcing-the-eks-cluster-games) which are designed help people test and improve their cloud security knowledge.

The objective is simple: you're given a terminal, an EKS (Elastic Kubernetes Service) hosted in AWS, which has some misconfiguration, allowing someone to gain more information about what is running on the cluster than is needed.

At the time of writing this, there are 5 challenges to test your knowledge.
