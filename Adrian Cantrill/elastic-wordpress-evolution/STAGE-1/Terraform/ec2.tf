/*************************************************\
 *                  WP INSTANCE                  *
\*************************************************/
resource "aws_key_pair" "keypair_wp" {
  key_name   = "WPKeyPair"
  public_key = file(pathexpand("~/.ssh/aws.pub"))
}


resource "aws_iam_role" "role_wp" {
  name = "WPRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonSSMFullAccess"]
}


resource "aws_iam_instance_profile" "instance_profile_wp" {
  name = "WPInstanceProfile"
  role = aws_iam_role.role_wp.name
}


resource "aws_instance" "instance_wp" {
  subnet_id     = aws_subnet.subnet_pub.0.id
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.keypair_wp.key_name

  vpc_security_group_ids = [aws_security_group.sg_wp.id]

  iam_instance_profile = aws_iam_instance_profile.instance_profile_wp.name

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-WP"
  })

  user_data = file("wp_userdata.sh")
}
