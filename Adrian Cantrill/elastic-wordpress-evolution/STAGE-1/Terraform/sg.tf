/*************************************************\
 *                SECURITY GROUPS                *
\*************************************************/

resource "aws_security_group" "sg_wp" {
  name        = "SGWordpress"
  description = "Security Group for allowing access to the WP instance."

  vpc_id = aws_vpc.a4l_vpc.id

  ingress {
    description = "Allow my IP HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${data.http.my_ip.response_body}/32"]
  }

  ingress {
    description = "Allow my IP SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${data.http.my_ip.response_body}/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-SGWordpress"
  })
}
