/*************************************************\
 *                 RDS INSTANCES                 *
\*************************************************/

resource "aws_db_subnet_group" "db_subnet_group_wp" {
  name        = "a4l subnet group"
  description = "A4L RDS Subnet Group"

  subnet_ids = aws_subnet.subnet_db[*].id

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-RDSSubnetGroupWordpress"
  })
}


resource "aws_db_instance" "db_wp" {
  allocated_storage = 10

  identifier_prefix = "a4l-rds-"

  db_name  = var.wp_dbname
  username = var.wp_dbuser
  password = var.wp_dbpassword

  ca_cert_identifier = "rds-ca-rsa4096-g1"

  instance_class = "db.t3.micro"
  engine         = "mysql"
  engine_version = var.rds_mysql_version

  db_subnet_group_name = aws_db_subnet_group.db_subnet_group_wp.name


  vpc_security_group_ids = [aws_security_group.sg_db.id]

  skip_final_snapshot = true

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-RDSWordpress"
  })
}
