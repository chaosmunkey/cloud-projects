# [Secret Seeker](https://eksclustergames.com/challenge/1)

> Jumpstart your quest by listing all the secrets in the cluster. Can you spot the flag among them?

###### Permissions

```json
{
    "secrets": [
        "get",
        "list"
    ]
}
```

## Process

One of the first steps is to see what user we're running as using the `kubectl whoami` command. The results of which show `system:serviceaccount:challenge1:service-account-challenge1`.

![](./images/whoami.png)


Now that we know who we're running as, let's see if we have any other permissions which we're not already aware of using `kubectl auth can-i --list`.

![](./images/can-i.png)

And by the looks of it, not so much. Looking at the permissions we do have, we are able to list the secrets and in doing so we stumble upon one by the name of `log-rotate`.

We can retrieve the secret using `kubectl get secrets log-rotate` and then specifying how to display; this can be either JSON or YAML. In this case it was JSON. The output from this command shows a *base64* encoded string with the key name of `flag`.

![](./images/get-secret.png)

In order to get the flag, we'll need to extract it from the secret and decode it. `kubectl` has the ability to pull out data using JSONPath syntax after which we can pass it into the `base64` binary and decode: `kubectl get secrets log-rotate -ojsonpath="{.data.flag}" | base64 -d`.

![](./images/flag.png)

---


## Conclusion

The ability to be able to list and get secrets can be quite dangerous, especially if you're able to do so within a production environment or namespace. To help reduce the risk of someone accidentally (or purposely) stumbling upon sensitive information, it would probably be best to only allow identities who need to list and/or get secrets that permission.
