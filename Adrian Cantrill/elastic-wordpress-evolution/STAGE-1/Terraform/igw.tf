resource "aws_internet_gateway" "a4l_igw" {
  vpc_id = aws_vpc.a4l_vpc.id

  tags = merge(local.common_tags, {
    name = "${local.resource_prefix}-igw"
  })
}


resource "aws_route_table_association" "a4l_igw_assoc" {
  count = length(var.pub_subnet_cidrs)

  subnet_id      = aws_subnet.subnet_pub[count.index].id
  route_table_id = aws_route_table.a4l_public_rt_table.id
}


resource "aws_route_table" "a4l_public_rt_table" {
  vpc_id = aws_vpc.a4l_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.a4l_igw.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.a4l_igw.id
  }

  tags = merge(local.common_tags, {
    name = "${local.resource_prefix}-igw-rt"
  })
}
