# Elastic Wordpress Evolution - Stage 1 - Single Server

This stage will utilise the infrastructure deployed in the previous stage to deploy a single EC2 instance to run a simple WordPress website. To achieve this monolithic deployment, the instance will have both the HTTP and database services required installed. The instance will also host any of the images.


By the end of this stage, the deployment will look similar to this, with the WordPress instance deployed to one of the public subnets:

![Stage 1](./images/Stage-1.png)

---

## Considerations

When the WordPress instance first deploys, it will be in an uninitialised state, meaning anyone can access the instance and set up the database. Given this problem, I have created a security group to allow only connections from my public IP to access the instance. This should not be a problem because this project is only meant as a training aid for myself.

The same consideration applies to restricting access to the SSH service running on the instance. Whereas in the real world, I would look to lock this down to either a bastion host or a small set of IP addresses.
