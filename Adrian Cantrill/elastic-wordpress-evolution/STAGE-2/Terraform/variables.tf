/*************************************************\
 *                TERRAFORM VARS                 *
\*************************************************/

variable "profile" {
  type        = string
  description = "Profile to use from the credentials file."

  default = "default"
}

variable "region" {
  type        = string
  description = "Region to deploy resources into."

  default = "us-east-1"
}

variable "billing_code" {
  type    = string
  default = "Billing code for resource tagging."
}

variable "company" {
  type        = string
  description = "Name of the company for resource tagging."

  default = "Animals4Life"
}

variable "environment" {
  type        = string
  description = "Environment resources are being deployed into."

  default = "dev"
}

variable "project" {
  type        = string
  description = "Name of the project for resource tagging."
}

/*************************************************\
 *                    VPC VARS                   *
\*************************************************/

variable "vpc_cidr" {
  type        = string
  description = "The CIDR block for the VPC."

  default = "10.16.0.0/16"
}

variable "azs" {
  type        = list(string)
  description = "Availability Zones to deploy into."

  default = ["a", "b", "c"]
}

variable "app_subnet_cidrs" {
  type        = list(string)
  description = "List of app subnet CIDR ranges."

  default = ["10.16.32.0/20", "10.16.96.0/20", "10.16.160.0/20"]
}

variable "app_ipv6" {
  type        = list(string)
  description = "List of app subnet IPv6 subnets"

  default = ["2", "6", "10"]
}

variable "db_subnet_cidrs" {
  type        = list(string)
  description = "List of DB subnet CIDR ranges."

  default = ["10.16.16.0/20", "10.16.80.0/20", "10.16.144.0/20"]
}

variable "db_ipv6" {
  type        = list(string)
  description = "List of DB subnet IPv6 subnets"

  default = ["1", "5", "9"]
}

variable "pub_subnet_cidrs" {
  type        = list(string)
  description = "List of web subnet CIDR ranges."

  default = ["10.16.48.0/20", "10.16.112.0/20", "10.16.176.0/20"]
}

variable "pub_ipv6" {
  type        = list(string)
  description = "List of web subnet IPv6 subnets"

  default = ["3", "7", "11"]
}


/*************************************************\
 *                 DB VARIABLES                  *
\*************************************************/
variable "wp_dbuser" {
  type        = string
  description = "Database user for WP."

  default = "a4lwordpressuser"
}

variable "wp_dbname" {
  type        = string
  description = "Database user for WP."

  default = "a4lwordpressdb"
}

variable "wp_dbendpoint" {
  type        = string
  description = "Database user for WP."

  default = "localhost"
}

variable "wp_dbpassword" {
  type        = string
  sensitive   = true
  description = "Database user for WP."
}

variable "wp_dbrootpassword" {
  type        = string
  sensitive   = true
  description = "Database user for WP."
}
