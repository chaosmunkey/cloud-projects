# Cloud Projects

This repo aims to showcase the projects that I have worked on to assist me in building up my skills in cloud computing, with the ultimate goal of gaining a role as a *cloud security engineer*.

While the most of these projects will be implemented on AWS, that does not mean I will not branch out to other cloud vendors in the future.

The plan is to implement the projects as much as possible using IaC (Infrastructure as Code), predominantly using Terraform, so that each project is deployable consistently and with a degree of idempotency.
