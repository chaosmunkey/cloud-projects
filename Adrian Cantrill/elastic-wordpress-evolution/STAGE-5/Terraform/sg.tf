/*************************************************\
 *                SECURITY GROUPS                *
\*************************************************/

resource "aws_security_group" "sg_wp" {
  name        = "SGWordpress"
  description = "Security Group for allowing access to the WP instance."

  vpc_id = aws_vpc.a4l_vpc.id

  ingress {
    description     = "Allow my IP HTTP"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_alb.id]
  }

  ingress {
    description = "Allow my IP SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${data.http.my_ip.response_body}/32"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-SGWordpress"
  })
}


resource "aws_security_group" "sg_alb" {
  name        = "SGA4LALB"
  description = "Security Group for the public facing ALB."

  vpc_id = aws_vpc.a4l_vpc.id

  ingress {
    description = "Allow my IP HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${data.http.my_ip.response_body}/32"]
    # cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-SGWordpressALB"
  })
}


resource "aws_security_group" "sg_db" {
  name        = "SGDB"
  description = "Security Group for allowing access to the DB instance."

  vpc_id = aws_vpc.a4l_vpc.id

  ingress {
    description     = "Control access to the DB."
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_wp.id]
  }

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-SGWordpressDB"
  })
}


resource "aws_security_group" "sg_efs" {
  name        = "SG EFS"
  description = "Security Group for allowing access to the EFS storage."

  vpc_id = aws_vpc.a4l_vpc.id

  ingress {
    description     = "Control access to EFS"
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    security_groups = [aws_security_group.sg_wp.id]
  }
  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-SGWordpressEFS"
  })
}
