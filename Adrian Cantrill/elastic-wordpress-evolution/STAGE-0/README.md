# Elastic Wordpress Evolution - Stage 0 - Initial Setup

This is the initial stage for this project which creates and sets up the empty shell of the VPC to host the Wordpress blog site. This initial deployment will consist of:
  1. VPC
  2. Subnets
  3. Internet Gateway (IGW)
  4. Route Tables
  5. Parameter Store Values

Once this initial stage has been deployed, the VPC will look something similar to this:

![](./images/Stage-0.png)


## Subnets

The VPC and subnet CIDRs will mirror that of Adrian's but is completely configurable by overriding the default variables set in the `terraform.tf` file.

**VPC CIDR:** 10.16.0.0/16

IPv4 Subnet CIDR|IPv6 CIDR|Label|AZ
-|-|-|-
10.16.16.0/20  | 01::/64 | DB-A  | US-East-1A
10.16.32.0/20  | 02::/64 | APP-A | US-East-1A
10.16.48.0/20  | 03::/64 | PUB-A | US-East-1A
10.16.80.0/20  | 05::/64 | DB-B  | US-East-1B
10.16.96.0/20  | 06::/64 | APP-B | US-East-1B
10.16.112.0/20 | 07::/64 | PUB-B | US-East-1B
10.16.144.0/20 | 09::/64 | DB-C  | US-East-1C
10.16.160.0/20 | 0A::/64 | APP-C | US-East-1C
10.16.176.0/20 | 0B::/64 | PUB-C | US-East-1C

There are some holes in the subnet allocation, this is a spare set of ranges for each AZ to allow for future expansion in this hypothetical scenario.
