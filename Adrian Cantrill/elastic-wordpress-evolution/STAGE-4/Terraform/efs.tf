/*************************************************\
 *                      EFS                      *
\*************************************************/

resource "aws_efs_file_system" "efs_wp" {
  creation_token = "efs-af4-wp"

  encrypted = true

  performance_mode = "generalPurpose"
  throughput_mode  = "bursting"

  lifecycle_policy {
    transition_to_ia = "AFTER_30_DAYS"
  }

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-EFS-Content"
  })
}

resource "aws_efs_mount_target" "efs_mt_wp" {
  count = length(var.app_subnet_cidrs)

  file_system_id = aws_efs_file_system.efs_wp.id

  subnet_id       = aws_subnet.subnet_app[count.index].id
  security_groups = [aws_security_group.sg_efs.id]
}
