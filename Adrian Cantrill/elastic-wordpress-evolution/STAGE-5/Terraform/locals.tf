/*************************************************\
 *                    LOCALS                     *
\*************************************************/

locals {

  common_tags = {
    billing_code = var.billing_code
    company      = var.company
    environment  = var.environment
    project      = "${var.company}-${var.project}"
  }

  resource_prefix = "${var.project}-${var.environment}"
}
