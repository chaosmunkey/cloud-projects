/*************************************************\
 *           APPLICATION LOAD BALANCER           *
\*************************************************/

# aws_alb is known as aws_lb. The functionality is identical.
resource "aws_alb" "alb_a4l_pub" {
  name = "alb-a4l-pub"

  internal           = false
  load_balancer_type = "application"

  security_groups = [aws_security_group.sg_alb.id]
  subnets         = aws_subnet.subnet_pub[*].id

  tags = {
    Name = "alb-a4l-pub"
  }
}


resource "aws_alb_target_group" "alb_tg_a4l" {
  name = "alb-tg-a4l-pub"

  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.a4l_vpc.id

  health_check {
    path     = "/"
    port     = 80
    protocol = "HTTP"
  }
}


resource "aws_alb_listener" "alb_listener_a4l" {
  load_balancer_arn = aws_alb.alb_a4l_pub.arn

  port     = 80
  protocol = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb_tg_a4l.arn
  }
}
