resource "aws_ssm_parameter" "ssm_wp_dbuser" {
  name  = "/A4L/Wordpress/DBUser"
  value = var.wp_dbuser

  tier      = "Standard"
  type      = "String"
  data_type = "text"

  tags = local.common_tags
}

resource "aws_ssm_parameter" "ssm_wp_dbname" {
  name  = "/A4L/Wordpress/DBName"
  value = var.wp_dbname

  tier      = "Standard"
  type      = "String"
  data_type = "text"

  tags = local.common_tags

}

resource "aws_ssm_parameter" "ssm_wp_dbendpoint" {
  name  = "/A4L/Wordpress/DBEndpoint"
  value = aws_db_instance.db_wp.address

  tier      = "Standard"
  type      = "String"
  data_type = "text"

  tags = local.common_tags
}

resource "aws_ssm_parameter" "ssm_wp_dbpassword" {
  name  = "/A4L/Wordpress/DBPassword"
  value = var.wp_dbpassword

  tier      = "Standard"
  type      = "SecureString"
  data_type = "text"

  tags = local.common_tags
}

resource "aws_ssm_parameter" "ssm_wp_dbrootpassword" {
  name  = "/A4L/Wordpress/DBRootPassword"
  value = var.wp_dbrootpassword

  tier      = "Standard"
  type      = "SecureString"
  data_type = "text"

  tags = local.common_tags
}

resource "aws_ssm_parameter" "ssm_efs_id" {
  name  = "/A4L/Wordpress/EFSFSID"
  value = aws_efs_file_system.efs_wp.id

  tier      = "Standard"
  type      = "String"
  data_type = "text"
}


resource "aws_ssm_parameter" "ssm_alb_dns_name" {
  name  = "/A4L/Wordpress/ALBDNSName"
  value = aws_alb.alb_a4l_pub.dns_name

  tier      = "Standard"
  type      = "String"
  data_type = "text"
}
