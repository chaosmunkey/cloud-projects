# Elastic Wordpress Evolution - Stage 2 - Launch Templates

This stage introduces Launch Templates to aid with deploying multiple instances of the WordPress instance. By using a Launch Template, users can deploy multiple instances of WordPress without having to worry about the details of which AMI to use, which subnet to place the instance into, or what security groups to assign.

An additional benefit to the launch template is its ability to be used with Auto Scaling Groups (ASGs), which will not be useful until the final stage of this project, where ASGs are introduced to horizontally scale the number of instances based on load.

By the end of this stage, the infrastructure will look pretty much the same, except for the additional Launch Template to assist with automating the deployment of the WordPress instance.

![Stage 2](./images/Stage-2.png)
