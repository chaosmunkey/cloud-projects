/*************************************************\
 *               AUTO SCALING GROUP              *
\*************************************************/

resource "aws_autoscaling_group" "asg_a4l_wp" {
  depends_on = [
    aws_efs_mount_target.efs_mt_wp,
    aws_db_instance.db_wp
  ]
  name = "asg-a4l-wp"

  min_size         = 1
  max_size         = 2
  desired_capacity = 1

  vpc_zone_identifier = aws_subnet.subnet_pub[*].id

  launch_template {
    id      = aws_launch_template.lt_wp.id
    version = "$Latest"
  }

  tag {
    key                 = "Name"
    value               = "Wordpress-ASG"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_attachment" "asg_a4l_wp_attach" {
  autoscaling_group_name = aws_autoscaling_group.asg_a4l_wp.id
  lb_target_group_arn    = aws_alb_target_group.alb_tg_a4l.arn
}


/*************************************************\
 *          ASG POLICY - CPU ABOVE 40%           *
\*************************************************/

resource "aws_cloudwatch_metric_alarm" "cw__high_cpu_alarm" {
  alarm_name  = "WP-High-CPU"
  metric_name = "CPUUtilization"
  namespace   = "AWS/EC2"
  statistic   = "Average"

  comparison_operator = "GreaterThanThreshold"
  threshold           = 40
  evaluation_periods  = 1
  period              = 300

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.asg_a4l_wp.name
  }

  alarm_actions = [aws_autoscaling_policy.asgp_a4l_wp_a40.arn]
}


resource "aws_autoscaling_policy" "asgp_a4l_wp_a40" {
  name = "HIGHCPU"

  policy_type        = "SimpleScaling"
  scaling_adjustment = 1
  adjustment_type    = "ChangeInCapacity"

  autoscaling_group_name = aws_autoscaling_group.asg_a4l_wp.name
}


/*************************************************\
 *          ASG POLICY - CPU BELOW 40%           *
\*************************************************/

resource "aws_cloudwatch_metric_alarm" "cw_low_cpu_alarm" {
  alarm_name  = "WP-Low-CPU"
  metric_name = "CPUUtilization"
  namespace   = "AWS/EC2"
  statistic   = "Average"

  comparison_operator = "LessThanThreshold"
  threshold           = 40
  evaluation_periods  = 1
  period              = 300

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.asg_a4l_wp.name
  }

  alarm_actions = [aws_autoscaling_policy.asgp_a4l_wp_b40.arn]
}


resource "aws_autoscaling_policy" "asgp_a4l_wp_b40" {
  name = "LOWCPU"

  policy_type        = "SimpleScaling"
  scaling_adjustment = -1
  adjustment_type    = "ChangeInCapacity"

  autoscaling_group_name = aws_autoscaling_group.asg_a4l_wp.name
}
