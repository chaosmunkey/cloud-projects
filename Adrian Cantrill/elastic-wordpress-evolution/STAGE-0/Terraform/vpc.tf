resource "aws_vpc" "a4l_vpc" {
  cidr_block = var.vpc_cidr

  instance_tenancy = "default"

  assign_generated_ipv6_cidr_block = true
  enable_dns_hostnames             = true

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-vpc"
  })
}

resource "aws_subnet" "subnet_app" {
  count = length(var.app_subnet_cidrs)

  vpc_id            = aws_vpc.a4l_vpc.id
  availability_zone = data.aws_availability_zones.azs.names[count.index]


  cidr_block      = element(var.app_subnet_cidrs, count.index)
  ipv6_cidr_block = cidrsubnet(aws_vpc.a4l_vpc.ipv6_cidr_block, 8, element(var.app_ipv6, count.index))

  assign_ipv6_address_on_creation = true

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-app-subnet-${element(var.azs, count.index)}"
  })
}

resource "aws_subnet" "subnet_db" {
  count = length(var.db_subnet_cidrs)

  vpc_id            = aws_vpc.a4l_vpc.id
  availability_zone = data.aws_availability_zones.azs.names[count.index]

  cidr_block      = element(var.db_subnet_cidrs, count.index)
  ipv6_cidr_block = cidrsubnet(aws_vpc.a4l_vpc.ipv6_cidr_block, 8, element(var.db_ipv6, count.index))

  assign_ipv6_address_on_creation = true

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-db-subnet-${element(var.azs, count.index)}"
  })
}

resource "aws_subnet" "subnet_pub" {
  count = length(var.pub_subnet_cidrs)

  vpc_id            = aws_vpc.a4l_vpc.id
  availability_zone = data.aws_availability_zones.azs.names[count.index]

  cidr_block      = element(var.pub_subnet_cidrs, count.index)
  ipv6_cidr_block = cidrsubnet(aws_vpc.a4l_vpc.ipv6_cidr_block, 8, element(var.pub_ipv6, count.index))

  map_public_ip_on_launch         = true
  assign_ipv6_address_on_creation = true

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-pub-subnet-${element(var.azs, count.index)}"
  })
}
