/*************************************************\
 *                  WP INSTANCE                  *
\*************************************************/
resource "aws_key_pair" "keypair_wp" {
  key_name   = "WPKeyPair"
  public_key = file(pathexpand("~/.ssh/aws.pub"))
}


resource "aws_iam_role" "role_wp" {
  name = "WPRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  managed_policy_arns = ["arn:aws:iam::aws:policy/AmazonSSMFullAccess"]
}


resource "aws_iam_instance_profile" "instance_profile_wp" {
  name = "WPInstanceProfile"
  role = aws_iam_role.role_wp.name
}


resource "aws_launch_template" "lt_wp" {

  name = "WPLaunchTemplate"

  instance_type = "t2.micro"
  image_id      = data.aws_ami.amazon_linux.id

  network_interfaces {
    subnet_id       = aws_subnet.subnet_pub.0.id
    security_groups = [aws_security_group.sg_wp.id]
  }

  iam_instance_profile {
    name = aws_iam_instance_profile.instance_profile_wp.name
  }

  key_name = aws_key_pair.keypair_wp.key_name

  user_data = filebase64("${path.root}/wp_userdata.sh")

  update_default_version = true

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-WPLaunchTemplate"
  })
}


resource "aws_instance" "instance_wp" {
  depends_on = [
    aws_db_instance.db_wp
  ]

  launch_template {
    id = aws_launch_template.lt_wp.id
  }

  tags = merge(local.common_tags, {
    Name = "${local.resource_prefix}-WP"
  })
}
